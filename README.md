# 使用CloudWatch紀錄waf日誌的方式

## 1. 建立一個IAM角色以便您的lambda函式可以從WAF服務獲取日誌并上傳到CloudWatch
        打開您的 IAM 控制台
        點擊左側 "Roles" -> "Create new role"
        選擇 "AWS Lambda" 然後 "Next Step"
        關聯 "AWSWAFReadOnlyAccess" 以及 "CloudWatchLogsFullAccess" 這兩個策略然後 "Next Step"
        為您的角色設置name (如 "lambda-waf-log") 以及 description 然後 "Create role"

## 2. 創建lambda函式
        打開您的 Lambda 控制台 (需要將區域切換為與您的WebACL相同的區域, 如果是套用在CloudFront的WebACL的話請使用N. Virginia區域)
        點擊 "Create function" -> "Author from scratch" 
        跳過trigger, 我們之後在配置
        設置function name, description, 使用 python 2.7 運行環境
        將附件中的代碼複製粘貼到lambda控制台中, "waf_log_global.py" 用於套用在CloudFront的WebACL， "waf_log_regional.py" 用於套用在ALB的WebACL
        修改下面的參數 "_WebACLId", "_Interval", "_OFFSET" 以符合您的需求，各個參數的作用您可以在代碼注釋中看到
        選擇您剛剛建立的 IAM 角色 (如 "lambda-waf-log") 于 "Lambda function handler and role " 中 -> "Existing role"
        點擊 "Next" -> "Create function"

## 3. 建立 CloudWatch event 來定期觸發 lambda 程式
        打開您的 CloudWatch 控制台并修改區域與您的 lambda 程式一致
        點擊左側 "Events" -> "Create rule"
        于 "Event Source" 中, 選擇 "Schedule"
        根據您的需要配置執行的頻率, 更多相關信息請您參考：https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html
        于 "Targets" 中, 點擊 "Add target" -> 勾選 "Lambda function" -> 選擇您剛剛建立的 lambda 程式 (如 "lambda-waf-log")
        剩下的選項保持默認然後點擊 "Configure details"
        于 Step 2 中, 配置一個rule的名字, 點擊 "Create rule"